import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:go_router/go_router.dart';
import 'package:lille_park/models/place.dart';
import 'package:lille_park/pages/home.dart';
import 'package:lille_park/utils/services.dart';
import 'package:provider/provider.dart';

import 'models/parking.dart';

Future<void> main() async {
  try {
    // On garde l'écran d'entrée jusqu'à ce que toutes les données soient initialisées
    FlutterNativeSplash.preserve(
        widgetsBinding: WidgetsFlutterBinding.ensureInitialized());
    runApp(FutureProvider(
      create: (context) async {
        // Données de paring récupérées en ligne
        List<Place> parkings = await getDataOnline();

        // Récupération des favoris
        final file = await localFile;

        // L'objectif ici est de récupérer les favoris et de les combiner avec les données collectées en ligne
        if (file.existsSync()) {
          String contents = file.readAsStringSync();
          var data = jsonDecode(contents);
          List<Place> favorites = List.from(data.map((json) {
            // Seuls les parkings contiennent la clé 'dispo'
            if (json.keys.contains('dispo')) {
              return Parking.fromLocalJSON(json);
            }
            return Place.fromJSON(json);
          }));

          // La liste contenant les données en ligne des parkings ainsi que les favoris enregistrés localement
          List<Place> places = List.from(parkings);
          for (final place in favorites) {
            bool contains = places.any((element) => element.id == place.id);
            if (contains) {
              int index = places.indexWhere((element) => element.id == place.id);
              places[index] = places[index].copyWith(favorite: true);
            } else {
              places.add(place);
            }
          }
          FlutterNativeSplash.remove();
          return AppState(places: places, favorites: favorites);
        }
        FlutterNativeSplash.remove();
        return AppState(places: parkings, favorites: []);
      },
      initialData: AppState(places: [], favorites: []),
      child: Consumer<AppState>(
          builder: (BuildContext context, AppState value, Widget? child) {
        if (value.places.isNotEmpty) {
          return (MaterialApp.router(
            theme: ThemeData(
                colorScheme: ColorScheme.fromSwatch().copyWith(
                    primary: const Color(0xff76d2fa),
                    secondary: const Color(0xFF010C7F),
                    tertiary: const Color(0xFF2AB8F6),
                    background: Colors.white),
                useMaterial3: true,
                fontFamily: 'Playfair Display'),
            routerConfig: GoRouter(routes: [
              GoRoute(
                path: '/',
                builder: (context, state) {
                  return const HomePage('LillePark');
                },
              ),
            ]),
          ));
        } else {
          return Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.white,
              color: Theme.of(context).colorScheme.primary,
            ),
          );
        }
      }),
    ));
  } catch (e, s) {
    // print(s);
  }
}
