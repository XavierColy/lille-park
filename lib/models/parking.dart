import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lille_park/models/place.dart';

/// Permet de représenter un place de parking
class Parking extends Place {
  final String ville;
  final String etat;
  final int dispo;
  final int max;
  final String affichage;
  final DateTime datemaj;

  const Parking(
      {required super.adresse,
      required this.ville,
      required this.etat,
      required this.dispo,
      required this.max,
      required this.datemaj,
      required super.id,
      required super.latLng,
      required super.libelle,
      required this.affichage,
      super.favorite});

  @override
  Parking copyWith(
      {String? id, LatLng? latLng, String? libelle, bool? favorite}) {
    return Parking(
      id: id ?? this.id,
      latLng: latLng ?? this.latLng,
      libelle: libelle ?? this.libelle,
      favorite: favorite ?? this.favorite,
      adresse: adresse,
      ville: ville,
      etat: etat,
      dispo: dispo,
      max: max,
      affichage: affichage,
      datemaj: datemaj,
    );
  }

  factory Parking.fromJSON(Map<String, dynamic> json) {
    double lat = json['geometry']['geometry']['coordinates'][1];
    double lng = json['geometry']['geometry']['coordinates'][0];
    return Parking(
        adresse: json['adresse'] as String,
        ville: json['ville'] as String,
        etat: json['etat'] as String,
        dispo: json['dispo'] as int,
        max: json['max'] as int,
        datemaj: DateTime.parse(json['datemaj']),
        id: json['id'] as String,
        latLng: LatLng(lat, lng),
        affichage: json['aff'] as String,
        libelle: json['libelle'] as String);
  }

  factory Parking.fromLocalJSON(Map<String, dynamic> json) {
    double lat = json['latLng'][0];
    double lng = json['latLng'][1];
    return Parking(
        adresse: json['adresse'] as String,
        ville: json['ville'] as String,
        etat: json['etat'] as String,
        dispo: json['dispo'] as int,
        max: json['max'] as int,
        datemaj: DateTime.parse(json['datemaj']),
        id: json['id'] as String,
        latLng: LatLng(lat, lng),
        affichage: json['affichage'] as String,
        libelle: json['libelle'] as String,
        favorite: json['favorite']);
  }

  @override
  Map<String, dynamic> toJSON() {
    return {
      'adresse': adresse,
      'ville': ville,
      'etat': etat,
      'dispo': dispo,
      'max': max,
      'datemaj': datemaj.toString(),
      'id': id,
      'latLng' : latLng,
      'affichage': affichage,
      'libelle': libelle,
      'favorite': favorite
    };
  }
}
