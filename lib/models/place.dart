import 'package:google_maps_flutter/google_maps_flutter.dart';

/// Permet de représenter une place sur la map
class Place {
  final String id;

  /// Représente les coordonnées de la place
  final LatLng latLng;
  final String libelle;
  final bool favorite;
  final String adresse;

  const Place(
      {required this.adresse,
      required this.id,
      required this.latLng,
      required this.libelle,
      this.favorite = false});

  double get latitude => latLng.latitude;

  double get longitude => latLng.longitude;

  Place copyWith(
      {String? id, LatLng? latLng, String? libelle, bool? favorite}) {
    return Place(
      id: id ?? this.id,
      latLng: latLng ?? this.latLng,
      libelle: libelle ?? this.libelle,
      favorite: favorite ?? this.favorite,
      adresse: adresse,
    );
  }

  factory Place.fromJSON(Map<String, dynamic> json) {
    double lat = json['latLng'][0];
    double lng = json['latLng'][1];
    return Place(
        adresse: json['adresse'],
        id: json['id'],
        latLng: LatLng(lat, lng),
        libelle: json['libelle'],
        favorite: json['favorite']);
  }

  Map<String, dynamic> toJSON() {
    return {
      'adresse': adresse,
      'id': id,
      'latLng' : latLng,
      'libelle': libelle,
      'favorite': favorite
    };
  }
}
