// Copyright 2020 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

import '../models/place.dart';
import '../widgets/map.dart';

class HomePage extends StatelessWidget {
  final String title;

  const HomePage(this.title, {super.key});

  @override
  build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        leading: Image.asset("assets/logos/icon.png"),
        title: Text(
          title,
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      body: const PlaceMap(),
    );
  }
}

/// C'est le contexte de l'application
class AppState extends ChangeNotifier {
  AppState({required this.places, required this.favorites});

  /// Contient tous les parkings ainsi que les destinations favorites de l'utilisateur
  List<Place> places;
  /// Contient uniquement les parkings et destinations favoris
  List<Place> favorites;

  void setPlaces(List<Place> newPlaces) {
    places = newPlaces;
    notifyListeners();
  }
}
