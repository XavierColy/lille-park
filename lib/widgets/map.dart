import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lille_park/models/parking.dart';
import 'package:lille_park/utils/services.dart';
import 'package:lille_park/widgets/bottom_sheet.dart';
import 'package:lille_park/widgets/search_bar.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';

import '../models/place.dart';
import '../pages/home.dart';
import 'side_floating_buttons.dart';

/// C'est la configuration de la carte
class MapConfiguration {
  final List<Place> places;
  final List<Place> favorites;

  const MapConfiguration({
    required this.places,
    required this.favorites,
  });

  @override
  int get hashCode => places.hashCode;

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }

    if (other.runtimeType != runtimeType) {
      return false;
    }

    return other is MapConfiguration &&
        other.places == places &&
        other.favorites == favorites;
  }

  static MapConfiguration of(AppState appState) {
    return MapConfiguration(
        places: appState.places, favorites: appState.favorites);
  }
}

/// L'élément principal de notre application
class PlaceMap extends StatefulWidget {
  const PlaceMap({super.key});

  @override
  State<PlaceMap> createState() => _PlaceMapState();
}

class _PlaceMapState extends State<PlaceMap> {
  Completer<GoogleMapController> mapController = Completer();

  /// C'est le type de la carte
  MapType _currentMapType = MapType.normal;

  /// Représente la position actuelle de l'utilisateur
  LocationData? _currentLocation;

  /// Représente la destination de l'utilisateur dans le cas où il chercherait la route menant à un parking ou une destination favorite
  LatLng? _destination;

  /// Ce sont les places sur la carte qui ont marqueur
  final Map<Marker, Place> _markedPlaces = <Marker, Place>{};

  final Set<Marker> _markers = {};

  MapConfiguration? _configuration;

  List<LatLng> _polylineCoordinates = [];

  bool _isCancelPolylineButtonVisible = false;

  @override
  void initState() {
    // On récupère la position de l'utilisateur dès le lancement de l'application et on y ajoute un marqueur
    getLocation().then((_) => _markers.add(Marker(
        markerId: const MarkerId("Moi"),
        infoWindow: const InfoWindow(title: "Vous êtes ici"),
        position: LatLng(
            _currentLocation!.latitude!, _currentLocation!.longitude!))));
    super.initState();
    context.read<AppState>().addListener(_watchMapConfigurationChanges);

    // Mise à jour des données toutes les 5 minutes
    Future.delayed(const Duration(minutes: 5), () {
      Timer.periodic(const Duration(minutes: 5), (timer) {
        final scaffoldMessenger = ScaffoldMessenger.of(context);
        _updateDataInRealTime()
            .then((value) => scaffoldMessenger.showSnackBar(const SnackBar(
                duration: Duration(seconds: 3),
                content: Text(
                  "Données rafraichies",
                  style: TextStyle(fontSize: 14),
                ))));
      });
    });
  }

  @override
  void dispose() {
    context.read<AppState>().removeListener(_watchMapConfigurationChanges);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _watchMapConfigurationChanges();
    return Builder(builder: (context) {
      return Center(
        child: Stack(
          children: [
            GoogleMap(
              onMapCreated: onMapCreated,
              initialCameraPosition: CameraPosition(
                target: _currentLocation?.latitude != null
                    ? LatLng(_currentLocation!.latitude!,
                        _currentLocation!.longitude!)
                    : const LatLng(50.691086648518, 3.1721265435623),
                zoom: 11.0,
              ),
              mapType: _currentMapType,
              markers: _markers,
              polylines: {
                Polyline(
                    polylineId: const PolylineId("route"),
                    points: _polylineCoordinates,
                    color: Theme.of(context).colorScheme.secondary,
                    width: 6)
              },
            ),
            SideFloatingButtons(
              onToggleMapTypePressed: _onToggleMapTypePressed,
              onToggleLocationButtonPressed: _onToggleLocationButtonPressed,
            ),
            MySearchBar(mapController: mapController),
            Visibility(
                visible: _isCancelPolylineButtonVisible,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  margin: const EdgeInsets.only(bottom: 15),
                  width: (MediaQuery.of(context).size.width - 40),
                  child: IntrinsicWidth(
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _isCancelPolylineButtonVisible = false;
                          _polylineCoordinates = [];
                        });
                      },
                      style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.red)),
                      child: const Text(
                        "Retour",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ))
          ],
        ),
      );
    });
  }

  //region MAP
  Future<void> onMapCreated(GoogleMapController controller) async {
    if (!context.mounted) return;
    final appState = Provider.of<AppState>(context, listen: false);
    mapController.complete(controller);

    var markers = <Marker>{};
    for (var place in appState.places) {
      markers.add(await _createPlaceMarker(place));
    }
    setState(() {
      _markers.addAll(markers);
    });
  }

  /// Fonction qui met à jour les marqueurs à chaque mise à jour des donné"es
  Future<void> _watchMapConfigurationChanges() async {
    // On compare l'état de l'appllication à la configuration pour savoir si les données ont changé
    final appState = context.read<AppState>();
    _configuration ??= MapConfiguration.of(appState);
    final newConfiguration = MapConfiguration.of(appState);

    if (_configuration != newConfiguration) {
      if (_configuration!.places != newConfiguration.places) {
        //Dacs ce cas, une destination favorite a forcément été supprimée
        if (_configuration!.places.length > newConfiguration.places.length) {
          for (final place in _configuration!.places) {
            if (!newConfiguration.places.contains(place)) {
              _markedPlaces.removeWhere((key, value) => value.id == place.id);
              _markers.removeWhere((element) =>
                  element.position == LatLng(place.latitude, place.longitude));
              setState(() {});
            }
          }
        }
        for (final place in newConfiguration.places) {
          final oldPlace =
              _configuration!.places.firstWhereOrNull((p) => p.id == place.id);
          if (oldPlace == null || oldPlace != place) {
            // New place or updated place.
            _updateOrCreatePlaceMarker(place: place);
          }
        }
      }
      _configuration = newConfiguration;
    }
  }

  Future<void> _zoomToShowPolylines(LatLng origin, LatLng destination) async {
    var controller = await mapController.future;

    var minLat = min(origin.latitude, destination.latitude);
    var maxLat = max(origin.latitude, destination.latitude);
    var minLong = min(origin.longitude, destination.longitude);
    var maxLong = max(origin.longitude, destination.longitude);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.animateCamera(
        CameraUpdate.newLatLngBounds(
          LatLngBounds(
            southwest: LatLng(minLat, minLong),
            northeast: LatLng(maxLat, maxLong),
          ),
          48.0,
        ),
      );
    });
  }

  /// Permet de récupérer la localisation actuelle de l'appareil
  Future<void> getLocation() async {
    Location location = Location();
    _currentLocation = await location.getLocation();

    location.onLocationChanged.listen((event) {
      _currentLocation = event;

      var marker =
          _markers.singleWhere((element) => element.markerId.value == "Moi");

      final updatedMarker = marker.copyWith(
          positionParam: LatLng(
              _currentLocation!.latitude!, _currentLocation!.longitude!));

      _markers.remove(marker);
      _markers.add(updatedMarker);
      setState(() {});
    });

    setState(() {});
  }

  //endregion

  //region MARKERS
  /// Crée les marqueurs
  Future<Marker> _createPlaceMarker(Place place) async {
    final marker = Marker(
        markerId: MarkerId(place.latLng.toString()),
        position: place.latLng,
        icon: await _getPlaceMarkerIcon(place),
        onTap: () {
          showModalBottomSheet(
                  context: context,
                  builder: (context) => MyBottomsheet(place: place))
              .then((location) {
            if (location != null) {
              setState(() {
                _destination = location;
                _getPolyPoints().then((_) {
                  if (_polylineCoordinates.isNotEmpty) {
                    _isCancelPolylineButtonVisible = true;
                    _zoomToShowPolylines(
                        LatLng(_currentLocation!.latitude!,
                            _currentLocation!.longitude!),
                        location);
                  }
                });
              });
            }
          });
        });
    _markedPlaces[marker] = place;
    return marker;
  }

  /// récupère l'icone du marqueur
  Future<BitmapDescriptor> _getPlaceMarkerIcon(Place place) {
    if (place.favorite) {
      if (place is Parking) {
        return BitmapDescriptor.fromAssetImage(
            createLocalImageConfiguration(context, size: const Size.square(32)),
            'assets/logos/parking_bitmap_red.png');
      } else {
        return BitmapDescriptor.fromAssetImage(
            createLocalImageConfiguration(context, size: const Size.square(32)),
            'assets/logos/heart.png');
      }
    } else {
      if (place is Parking) {
        return BitmapDescriptor.fromAssetImage(
            createLocalImageConfiguration(context, size: const Size.square(32)),
            'assets/logos/parking_bitmap.png');
      }
    }
    return Future.value(BitmapDescriptor.defaultMarker);
  }

  /// Permet de mettre à jour les marqueurs
  void _updateOrCreatePlaceMarker({required Place place}) async {
    if (_markedPlaces.values.contains(place)) {
      var marker = _markedPlaces.keys
          .singleWhere((value) => _markedPlaces[value]!.id == place.id);

      final updatedMarker = marker.copyWith(
          iconParam: await _getPlaceMarkerIcon(place),
          onTapParam: () {
            showModalBottomSheet(
                context: context,
                builder: (context) => MyBottomsheet(place: place as Parking));
          });
      setState(() {
        _updateMarker(
            marker: marker, updatedMarker: updatedMarker, place: place);
      });
    } else {
      _markers.add(await _createPlaceMarker(place));
      setState(() {});
    }
  }

  void _updateMarker({
    required Marker? marker,
    required Marker updatedMarker,
    required Place place,
  }) {
    _markers.remove(marker);
    _markedPlaces.remove(marker);

    _markers.add(updatedMarker);
    _markedPlaces[updatedMarker] = place;
  }

  //endregion

  //region PRIVATE FUNCTIONS
  void _onToggleMapTypePressed() {
    final nextType =
        MapType.values[(_currentMapType.index + 1) % MapType.values.length];

    setState(() {
      _currentMapType = nextType;
    });
  }

  /// Repositionnne la caméra à la position actuelle de l'utilisateur
  Future<void> _onToggleLocationButtonPressed() async {
    if (_currentLocation!.latitude != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        var center =
            LatLng(_currentLocation!.latitude!, _currentLocation!.longitude!);
        var controller = await mapController.future;
        controller.animateCamera(CameraUpdate.newCameraPosition(
            CameraPosition(target: center, zoom: 13)));
      });
    }
  }

  /// Permet de mettre à jour l'application en temps réel
  Future<void> _updateDataInRealTime() async {
    final appState = Provider.of<AppState>(context, listen: false);
    // Données de paring récupérées en ligne
    List<Place> parkings = await getDataOnline();

    // Récupération des favoris
    final file = await localFile;

    if (file.existsSync()) {
      String contents = file.readAsStringSync();
      var data = jsonDecode(contents);
      List<Place> favorites = List.from(data.map((json) {
        if (json.keys.contains('dispo')) {
          return Parking.fromLocalJSON(json);
        }
        return Place.fromJSON(json);
      }));

      // La liste contenant les données en ligne des parkings ainsi que les favoris enregistrés localement
      List<Place> places = List.from(parkings);
      for (final place in favorites) {
        // On vérifie si la liste des places contient des favoris
        bool contains = places.any((element) => element.id == place.id);
        if (contains) {
          int index = places.indexWhere((element) => element.id == place.id);
          places[index] = places[index].copyWith(favorite: true);
        } else {
          places.add(place);
        }
      }
      appState.favorites = favorites;
      appState.setPlaces(places);
    }
    appState.setPlaces(parkings);
  }

  /// Récupère le tracé de route vers une destination
  Future<void> _getPolyPoints() async {
    PolylinePoints polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      "AIzaSyDyYwl6G7PYanYdLKzX71OL93p-cIsJ5Yw",
      PointLatLng(_currentLocation!.latitude!, _currentLocation!.longitude!),
      PointLatLng(_destination!.latitude, _destination!.longitude),
    );
    if (result.points.isNotEmpty) {
      for (var point in result.points) {
        _polylineCoordinates = [];
        _polylineCoordinates.add(
          LatLng(point.latitude, point.longitude),
        );
      }
      setState(() {});
    }
  }
//endregion
}
