import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lille_park/models/place.dart';
import 'package:lille_park/utils/services.dart';
import 'package:map_location_picker/map_location_picker.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

import '../pages/home.dart';

class MySearchBar extends StatefulWidget {
  final Completer<GoogleMapController> mapController;

  const MySearchBar({super.key, required this.mapController});

  @override
  State<MySearchBar> createState() => _MySearchBarState();
}

class _MySearchBarState extends State<MySearchBar> {
  late PlaceDetails chosenPlace;
  bool isFavoritePlaceButtonActive = false;
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    _controller.addListener(_onSearchBarChange);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        PlacesAutocomplete(
          apiKey: "AIzaSyDyYwl6G7PYanYdLKzX71OL93p-cIsJ5Yw",
          mounted: mounted,
          language: 'fr',
          searchHintText: 'Chercher ici',
          searchController: _controller,
          hideBackButton: true,
          hideSuggestionsOnKeyboardHide: true,
          hideOnLoading: true,
          hideOnEmpty: true,
          region: 'fr',
          onGetDetailsByPlaceId: (PlacesDetailsResponse? result) {
            if (result != null) {
              chosenPlace = result.result;
              var lat = chosenPlace.geometry!.location.lat;
              var lng = chosenPlace.geometry!.location.lng;
              WidgetsBinding.instance.addPostFrameCallback((_) async {
                var center = LatLng(lat, lng);
                var controller = await widget.mapController.future;
                controller.animateCamera(CameraUpdate.newCameraPosition(
                    CameraPosition(target: center, zoom: 13)));
              });

              setState(() {
                isFavoritePlaceButtonActive = true;
              });
            }
          },
        ),
        const SizedBox(height: 8),
        Visibility(
            visible: isFavoritePlaceButtonActive,
            child: SizedBox(
              width: MediaQuery.of(context).size.width - 20,
              child: ElevatedButton(
                onPressed: _onPressFavoritePlaceButton,
                style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Colors.green)),
                child: const Text(
                  "Ajouter cette adresse aux favoris",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )),
      ],
    );
  }

  void _onSearchBarChange() {
    setState(() {
      isFavoritePlaceButtonActive = false;
    });
  }

  Future<void> _onPressFavoritePlaceButton() async {
    var state = Provider.of<AppState>(context, listen: false);
    var lat = chosenPlace.geometry!.location.lat;
    var lng = chosenPlace.geometry!.location.lng;
    var newFavPlace = Place(
        id: const Uuid().v1(),
        latLng: LatLng(lat, lng),
        libelle: chosenPlace.name,
        favorite: true,
        adresse: chosenPlace.formattedAddress!);
    var finalPlaces = List<Place>.from(state.places)..add(newFavPlace);

    // On ajoute la place dans les favoris
    state.favorites.add(newFavPlace);
    //Maintenant, on l'enregistre localement
    String jsonData =
        jsonEncode(state.favorites.map((e) => e.toJSON()).toList());
    File file = await localFile;
    file.writeAsStringSync(jsonData, mode: FileMode.write);

    isFavoritePlaceButtonActive = false;
    state.setPlaces(finalPlaces);
    setState(() {});
  }
}
