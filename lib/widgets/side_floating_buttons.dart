import 'package:flutter/material.dart';

class SideFloatingButtons extends StatelessWidget {
  final VoidCallback onToggleMapTypePressed;
  final VoidCallback onToggleLocationButtonPressed;

  const SideFloatingButtons(
      {super.key,
      required this.onToggleMapTypePressed,
      required this.onToggleLocationButtonPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      margin: const EdgeInsets.only(right: 14.0),
      child: IntrinsicHeight(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(99)),
                  border: Border.all(color: Colors.black, width: 1),
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                        blurRadius: 20,
                        offset: Offset(6, 4),
                        color: Colors.grey,
                        spreadRadius: 1)
                  ]),
              child: IconButton(
                onPressed: onToggleMapTypePressed,
                icon: const Icon(Icons.layers, size: 28.0),
              ),
            ),
            const SizedBox(height: 12.0),
            // Location button
            Container(
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(99)),
                  border: Border.all(color: Colors.black, width: 1),
                  boxShadow: const [
                    BoxShadow(
                        blurRadius: 20,
                        offset: Offset(6, 4),
                        color: Colors.grey,
                        spreadRadius: 1)
                  ],
                  color: Colors.white),
              child: IconButton(
                onPressed: onToggleLocationButtonPressed,
                icon: const Icon(Icons.my_location, size: 28.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
