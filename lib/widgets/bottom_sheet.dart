import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import '../models/parking.dart';
import '../models/place.dart';
import '../pages/home.dart';
import '../utils/services.dart';

class MyBottomsheet extends StatefulWidget {
  final Place place;

  const MyBottomsheet({super.key, required this.place});

  @override
  State<MyBottomsheet> createState() => _MyBottomsheetState();
}

class _MyBottomsheetState extends State<MyBottomsheet> {
  @override
  Widget build(BuildContext context) {
    var adrresse = widget.place is Parking
        ? "${widget.place.adresse}, ${(widget.place as Parking).ville}"
        : widget.place.adresse;
    var dispo = widget.place is Parking
        ? "${(widget.place as Parking).dispo}/${(widget.place as Parking).max} places"
        : "";
    var aff = widget.place is Parking
        ? "Affichage panneau: ${(widget.place as Parking).affichage}"
        : "";

    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.primary,
          borderRadius: const BorderRadius.vertical(top: Radius.circular(20))),
      child: IntrinsicHeight(
        child: Column(
          children: [
            Center(
                child: Text(
              widget.place.libelle,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(fontSize: 22, height: 2),
            )),
            Container(
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(20))),
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                    decoration: BoxDecoration(
                        color:
                            widget.place.favorite ? Colors.red : Colors.green,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(12)),
                        border: Border.all(
                            color: widget.place.favorite
                                ? Colors.red
                                : Colors.green,
                            width: 1)),
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: () => Navigator.pop(
                              context,
                              LatLng(widget.place.latitude,
                                  widget.place.longitude)),
                          child: Card(
                            margin: const EdgeInsets.all(0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    const SizedBox(width: 4),
                                    Image.asset(
                                      "assets/logos/icon.png",
                                      width: 40,
                                    ),
                                    const SizedBox(width: 10),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        IntrinsicWidth(
                                          child: Text(
                                            adrresse,
                                            style: const TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        widget.place is Parking
                                            ? Text(
                                                aff,
                                                style: const TextStyle(
                                                    fontSize: 12),
                                              )
                                            : const SizedBox()
                                      ],
                                    ),
                                    // Juste pour créer de l'espace
                                    const Expanded(child: SizedBox(width: 0)),
                                    widget.place is Parking
                                        ? Text(widget.place.id,
                                            style:
                                                const TextStyle(fontSize: 12))
                                        : const SizedBox(),
                                    SizedBox(
                                        width: widget.place is Parking ? 4 : 0),
                                  ],
                                ),
                                widget.place is Parking
                                    ? const Divider(
                                        height: 16,
                                        indent: 10,
                                        endIndent: 10,
                                        thickness: 1,
                                        color: Colors.grey,
                                      )
                                    : const SizedBox(),
                                widget.place is Parking
                                    ? Row(
                                        children: [
                                          const SizedBox(width: 16),
                                          Expanded(
                                              flex: 1,
                                              child: Text(
                                                  "Etat: ${(widget.place as Parking).etat}",
                                                  style: const TextStyle(
                                                      fontSize: 14))),
                                          Text(dispo,
                                              style: const TextStyle(
                                                  fontSize: 16)),
                                          const SizedBox(width: 8)
                                        ],
                                      )
                                    : const SizedBox(),
                                const SizedBox(height: 4)
                              ],
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: TextButton(
                                  onPressed: _onChangeFavorite,
                                  child: Text(
                                      widget.place.favorite
                                          ? "Supprimer des favoris"
                                          : "Ajouter aux favoris",
                                      style: const TextStyle(color: Colors.white))),
                            ),
                            const SizedBox()
                          ],
                        ),
                      ],
                    ),
                  ),
                  widget.place is Parking
                      ? Text(
                          "Date de mise à jour ${DateFormat('dd/MM/yy HH:mm').format((widget.place as Parking).datemaj)}",
                          style: const TextStyle(fontSize: 13),
                        )
                      : const SizedBox(),
                  const SizedBox(height: 10)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _onChangeFavorite() async {
    var state = Provider.of<AppState>(context, listen: false);
    List<Place> finalPlaces;
    if (widget.place is Parking) {
      var newParking = widget.place.copyWith(favorite: !widget.place.favorite);
      finalPlaces = List<Place>.from(state.places);
      final index =
          finalPlaces.indexWhere((place) => place.id == newParking.id);
      finalPlaces[index] = newParking;
      !widget.place.favorite
          ? state.favorites.add(newParking)
          : state.favorites.remove(widget.place);
    } else {
      finalPlaces = List<Place>.from(state.places);
      finalPlaces.removeWhere((element) => element.id == widget.place.id);
      state.favorites.remove(widget.place);
    }
    //Maintenant, on l'enregistre localement
    File file = await localFile;
    String jsonData =
        jsonEncode(state.favorites.map((e) => e.toJSON()).toList());
    file.writeAsStringSync(jsonData, mode: FileMode.write);
    state.setPlaces(finalPlaces);
    Navigator.pop(context);
  }
}
