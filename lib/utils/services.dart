import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:lille_park/models/place.dart';
import 'package:path_provider/path_provider.dart';

import '../models/parking.dart';

/// Permet de récupérer les données sur les parkings en ligne <br>
/// Récupère uniquement les parkings ouverts et avec des places disponibles
Future<List<Place>> getDataOnline() async {
  var url =
      "https://opendata.lillemetropole.fr/api/explore/v2.1/catalog/datasets/disponibilite-parkings/records?where=dispo%20%3E%200%20and%20etat%20!%3D%20%22FERME%22&limit=28";
  final response = await http.get(Uri.parse(url));
  if (response.statusCode == 200) {
    var data = jsonDecode(response.body);
    return List<Place>.from(
        data['results'].map((model) => Parking.fromJSON(model)));
  } else {
    throw Exception("Erreur dans le chargement des données en ligne");
  }
}

/// Permet de récupérer le chemin d'accès au répertoire des documents
Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();
  return directory.path;
}

Future<File> get localFile async {
  final path = await _localPath;
  return File("$path/favorites.txt");
}
