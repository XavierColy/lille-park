# Lille Park

## Description

L'application permet de voir les disponibilités de parkings dans la métropole Lilloise.

## Visuels

<table>
<tr><td>Video</td></tr>
<tr><td> ![preview.mp4](assets%2Fimages%2Fpreview.mp4) </td></tr>
</table>

Vous pouvez voir les maquettes de l'application directement sur [figma](https://www.figma.com/file/oh74JSgLAnXnZErL2jRzmM/LillePark?type=design&node-id=1%3A2&mode=design&t=kRinE4rpLtul5jTh-1).

## Comment l'utiliser

L'application permet de voir uniquement <ins>les parkings ouverts et pour lesquels il reste des places libres</ins>. <br>
Les parkings sont de bases de couleur bleue. Seuls les parkings favoris de l'utilisateur sont en rouge. De même, il est possible 
d'ajouter des destinations favorites. Ces dernières sont représentées par des coeurs sur la carte. <br>
En cliquant sur le sélecteur d'un parking, vous pourrez voir toutes les informations de celui-ci notamment le nombre de places restantes et la date de mise 
à jour des données sur le [site](https://opendata.lillemetropole.fr/explore/dataset/disponibilite-parkings/information/). 
Dans l'application, les données sont mises à jour **toutes les 5 minutes**. <br>


**Petite astuce**: Cliquez sur l'adresse d'un parking ou d'une destination favorite pour tracer une route y menant depuis votre position actuelle!!!
<table>
<tr>
<td>![Display_favorite_place.png](assets%2Fimages%2FDisplay_favorite_place.png)</td>
<td>![Draw_route.png](assets%2Fimages%2FDraw_route.png)</td>
</tr>
</table>

## Fonctionnalités

Cette application utilise plusieurs packages notamment [map_location_picker](https://github.com/mohesu/map_location_picker/tree/master) pour la barre de 
recherche des places, flutter_google_maps pour la carte, **location** pour avoir la position exacte.
